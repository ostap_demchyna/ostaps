package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadProperties {

    static Properties properties = new Properties();

    private final static String ACCESS_PROP = "/env.properties";

    public static String getEnvPropertyByPropertyName(String propName){
        try {
            InputStream inputProp = LoadProperties.class.getResourceAsStream(ACCESS_PROP);
            properties.load(inputProp);
        }catch (IOException io){
            io.printStackTrace();
        }
        return properties.getProperty(propName);
    }
}
