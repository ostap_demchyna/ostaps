package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ComputerPage extends BasePage {

    @FindBy(id = "company")
    private WebElement companyDropDown;
    @FindBy(id = "name")
    private WebElement computerNameField;
    @FindBy(xpath = "//div[@class='actions']//input[@type='submit']")
    private WebElement createSaveComputerButton;
    @FindBy(xpath = "//input[@value='Delete this computer']")
    private WebElement deleteComputerButton;
    @FindBy(id = "discontinued")
    private WebElement discontinueddDateField;
    @FindBy(id = "introduced")
    private WebElement introducedDateField;

    public WebElement getCompanyDropDown() {
        return companyDropDown;
    }

    public WebElement getComputerNameField() {
        return computerNameField;
    }

    public WebElement getCreateSaveComputerButton() {
        return createSaveComputerButton;
    }

    public WebElement getDeleteComputerButton() {
        return deleteComputerButton;
    }

    public WebElement getDiscontinuedDateField() {
        return discontinueddDateField;
    }

    public WebElement getIntroducedDateField() {
        return introducedDateField;
    }
}
