package pages;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.BaseUtil;

public class BasePage {

    final WebDriver driver = BaseUtil.getDriver();

    public BasePage() {
        PageFactory.initElements(driver, this);
    }

    public void waitForElementToBeLoaded(WebElement element) {
        new WebDriverWait(driver, 15).until(visibilityOfElementLocated(element));
    }

    private static ExpectedCondition<WebElement> visibilityOfElementLocated(final WebElement element) {
        return new ExpectedCondition<WebElement>() {

            public WebElement apply(WebDriver driver) {
                try {
                    return elementIfVisible(element);
                } catch (StaleElementReferenceException var3) {
                    return null;
                }
            }

            public String toString() {
                return "visibility of element named " + element.toString();
            }
        };
    }

    private static WebElement elementIfVisible(WebElement element) {
        return element.isDisplayed() ? element : null;
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = driver1 ->
                ((JavascriptExecutor) driver1).executeScript("return document.readyState").toString()
                        .equals("complete");
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }
}
