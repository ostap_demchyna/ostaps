package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ComputersMainPage extends BasePage {

    @FindBy(id = "add")
    private WebElement addNewComputerButton;
    private String columnSelector = "//a[contains(text(),'%s')]/ancestor::table//a[contains(text(),'%s')]/ancestor::tr/td[%s]";
    @FindBy(id = "searchsubmit")
    private WebElement filterByNameButton;
    @FindBy(id = "searchbox")
    private WebElement searchField;
    @FindBy(xpath = "//div[@class='alert-message warning']")
    private WebElement message;
    @FindBy(xpath = "//table")
    private WebElement table;

    public WebElement getAddNewComputerButton() {
        return addNewComputerButton;
    }

    public WebElement getCompanyNameFromTableByComputerName(String computerName) {
        return driver.findElement(By.xpath(String.format(columnSelector, "Company", computerName, "4")));
    }

    public WebElement getComputerByName(String computerName) {
        return driver.findElement(By.xpath(String.format("//a[contains(text(),'%s')]", computerName)));
    }

    public WebElement getDiscontinuedDateFromTableByComputerName(String computerName) {
        return driver.findElement(By.xpath(String.format(columnSelector, "Discontinued", computerName, "3")));
    }

    public WebElement getFilterByNameButton() {
        return filterByNameButton;
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public WebElement getIntroducedDateFromTableByComputerName(String computerName) {
        return driver.findElement(By.xpath(String.format(columnSelector, "Introduced", computerName, "2")));
    }

    public WebElement getMessage() {
        return message;
    }

    public WebElement getTable() {
        return table;
    }
}
