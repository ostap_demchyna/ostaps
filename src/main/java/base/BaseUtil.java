package base;

import org.openqa.selenium.WebDriver;

public class BaseUtil {

    private static WebDriver driver;

    public static synchronized WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(WebDriver driver) {
        BaseUtil.driver = driver;
    }
}
