import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = { "src/test/resources/com/herokuapp" },
        format = { "pretty", "html:reports/test-report" }
)

public class TestRunner {

}
