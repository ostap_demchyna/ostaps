package webStepDefinition;

import static org.hamcrest.core.Is.is;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Assert;

import base.BaseUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.BasePage;
import pages.ComputerPage;
import pages.ComputersMainPage;

public class webStepDefinitions {

    private static final String DATE_CREATION_PATERN = "yyyy-MM-dd";
    private static final String DATE_TABLE_PATERN = "d MMM yyyy";
    private static final String MESSAGE_FRAGMENT = "Computer with name ";
    private BaseUtil base;
    private BasePage basePage = new BasePage();
    private ComputerPage computerPage = new ComputerPage();
    private ComputersMainPage computersMainPage = new ComputersMainPage();

    public webStepDefinitions(BaseUtil base) {
        this.base = base;
    }

    private String parseDate(String date, String patern) {
        return LocalDate.parse(date).format(DateTimeFormatter.ofPattern(patern));
    }

    @When("^user click on Add a new computer button$")
    public void userClickOnAddANewComputerButton() {
        computersMainPage.getAddNewComputerButton().click();
    }

    @And("^user click on Create/Save this computer name button$")
    public void userClickOnCreateThisComputerName() {
        computerPage.getCreateSaveComputerButton().click();
    }

    @And("^user click on Delete this computer button$")
    public void userClickOnDeleteThisComputerButton()  {
        computerPage.getDeleteComputerButton().click();
    }

    @And("^user click on '(.*)' link in the table$")
    public void userClickOnNameLinkInTheTable(final String computerName) {
        computersMainPage.getComputerByName(computerName).click();
    }

    @And("^user fill Computer Name field with '(.*)' name$")
    public void userFillComputerNameFieldWithName(final String computerName) {
        computerPage.getComputerNameField().clear();
        computerPage.getComputerNameField().sendKeys(computerName);
    }

    @And("^user fill Discontinued date with '(.*)' date$")
    public void userFillDiscontinuedDateWithYearMonthAndDay(final String date) {
        computerPage.getDiscontinuedDateField().clear();
        computerPage.getDiscontinuedDateField().sendKeys(parseDate(date, DATE_CREATION_PATERN));
    }

    @And("^user fill Introduced date with '(.*)' date$")
    public void userFillIntroducedDateWithYearMonthAndDay(final String date) {
        computerPage.getIntroducedDateField().clear();
        computerPage.getIntroducedDateField().sendKeys(parseDate(date, DATE_CREATION_PATERN));
    }

    @Given("^user is navigated on herokuapp web page$")
    public void userIsNavigatedOnHerokuappWebPage() {

    }

    @When("^user search for '(.*)' computer name$")
    public void userSearchForName(final String computerName) {
        computersMainPage.getSearchField().sendKeys(computerName);
        computersMainPage.getFilterByNameButton().click();
        basePage.waitForElementToBeLoaded(computersMainPage.getComputerByName(computerName));
    }

    @Then("^user see '(.*)' message$")
    public void userSeeDoneComputerIBMHasBeenCreatedMessage(final String message) {
        Assert.assertThat("Message is not recognized", computersMainPage.getMessage().getText(), is(message));
    }

    @And("^user see that computer with '(.*)' name has '(.*)' company name$")
    public void userSeeThatComputerWithIBMNameHasSonyCompanyName(final String computerName, final String companyName) {
        Assert.assertThat(MESSAGE_FRAGMENT + computerName + " have other company name from " + companyName,
                computersMainPage.getCompanyNameFromTableByComputerName(computerName).getText(), is(companyName));
    }

    @And("^user see that computer with '(.*)' name has '(.*)' date as Discontinued date$")
    public void userSeeThatComputerWithNameHasYearMonthAndDayAsDiscontinuedDate(final String computerName,
            final String date) {
        Assert.assertThat(MESSAGE_FRAGMENT + computerName + " have other Discontinued date",
                computersMainPage.getDiscontinuedDateFromTableByComputerName(computerName).getText(),
                is(parseDate(date, DATE_TABLE_PATERN)));
    }

    @And("^user see that computer with '(.*)' name has '(.*)' date as Introduced date$")
    public void userSeeThatComputerWithNameHasYearMonthAndDayAsIntroducedDate(final String computerName,
            final String date) {
        Assert.assertThat(MESSAGE_FRAGMENT + computerName + " have other Introduced date",
                computersMainPage.getIntroducedDateFromTableByComputerName(computerName).getText(),
                is(parseDate(date, DATE_TABLE_PATERN)));
    }

    @Then("^user see that computer with '(.*)' name is present in computer table$")
    public void userSeeThatComputerWithNameIsPresentInComputerTable(final String computerName) {
        Assert.assertTrue(MESSAGE_FRAGMENT + computerName + " does not present in the table",
                computersMainPage.getComputerByName(computerName).isDisplayed());
    }

    @And("^user select '(.*)' company from Company drop-down$")
    public void userSelectSonyCompanyFromCompanyDropDown(final String companyName) {
        computerPage.getCompanyDropDown().sendKeys(companyName);
    }
}
