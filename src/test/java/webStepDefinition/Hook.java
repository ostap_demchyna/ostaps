package webStepDefinition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;

import base.BaseUtil;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import util.LoadProperties;

public class Hook extends BaseUtil {

    @Before
    public void InitializeTest() {
        ChromeDriverManager.getInstance().setup();
        setDriver(new ChromeDriver());
        getDriver().manage().window().maximize();
        getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        getDriver().navigate().to(LoadProperties.getEnvPropertyByPropertyName("HEROKUAPP_URL"));
    }

    @After
    public void TearDownTest(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                scenario.write("Current Page URL is " + getDriver().getCurrentUrl());
                byte[] screenshot = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (ClassCastException cce) {
                cce.printStackTrace();
            }
            System.err.println(scenario.getName());
        }
        getDriver().quit();
    }
}
