@update
Feature:Verify computer edition
  I us user
  I want to be able edit computer data
  To be able operate computer content

  Scenario: pre condition
    Given user is navigated on herokuapp web page
    When user click on Add a new computer button
    And user fill Computer Name field with 'IBM_TEST' name
    And user fill Introduced date with '2018-03-16' date
    And user fill Discontinued date with '2018-03-15' date
    And user select 'Sony' company from Company drop-down
    And user click on Create/Save this computer name button

  Scenario: computer edition
    Given user is navigated on herokuapp web page
    When user search for 'IBM_TEST' computer name
    And user click on 'IBM_TEST' link in the table
    And user fill Computer Name field with 'IBM_TEST_EDITED' name
    And user fill Discontinued date with '2017-04-16' date
    And user fill Introduced date with '2017-04-17' date
    And user select 'Nokia' company from Company drop-down
    And user click on Create/Save this computer name button
    Then user see 'Done! Computer IBM_TEST_EDITED has been updated' message

  Scenario: Verify computer parameters
    Given user is navigated on herokuapp web page
    When user search for 'IBM_TEST_EDITED' computer name
    Then user see that computer with 'IBM_TEST_EDITED' name is present in computer table
    And user see that computer with 'IBM_TEST_EDITED' name has '2017-04-17' date as Introduced date
    And user see that computer with 'IBM_TEST_EDITED' name has '2017-04-16' date as Discontinued date
    And user see that computer with 'IBM_TEST_EDITED' name has 'Nokia' company name

  Scenario: post condition
    Given user is navigated on herokuapp web page
    When user search for 'IBM_TEST_EDITED' computer name
    And user click on 'IBM_TEST_EDITED' link in the table
    And user click on Delete this computer button
