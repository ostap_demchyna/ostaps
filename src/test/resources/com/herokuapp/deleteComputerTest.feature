@delete
Feature: Verify computer deletion
  I as user
  I want to be able delete computer
  To get rid of computer if it is not needed

  Scenario: Computer creation
    Given user is navigated on herokuapp web page
    When user click on Add a new computer button
    And user fill Computer Name field with 'IBM_TEST' name
    And user fill Introduced date with '2018-03-16' date
    And user fill Discontinued date with '2018-03-15' date
    And user select 'Sony' company from Company drop-down
    And user click on Create/Save this computer name button

  Scenario: computer deletion
    Given user is navigated on herokuapp web page
    When user search for 'IBM_TEST' computer name
    And user click on 'IBM_TEST' link in the table
    And user click on Delete this computer button
    Then user see 'Done! Computer has been deleted' message
