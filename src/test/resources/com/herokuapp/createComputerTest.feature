@create
Feature: Verify computer creation
  I us user
  I want to create new computer
  To make other manipulation with computers

  Scenario: Computer creation
    Given user is navigated on herokuapp web page
    When user click on Add a new computer button
    And user fill Computer Name field with 'IBM_TEST' name
    And user fill Introduced date with '2018-03-16' date
    And user fill Discontinued date with '2018-03-15' date
    And user select 'Sony' company from Company drop-down
    And user click on Create/Save this computer name button
    Then user see 'Done! Computer IBM_TEST has been createddddd' message

  Scenario: Verify computer parameters
    Given user is navigated on herokuapp web page
    When user search for 'IBM_TEST' computer name
    Then user see that computer with 'IBM_TEST' name is present in computer table
    And user see that computer with 'IBM_TEST' name has '2018-03-16' date as Introduced date
    And user see that computer with 'IBM_TEST' name has '2018-03-15' date as Discontinued date
    And user see that computer with 'IBM_TEST' name has 'Sony' company name

  Scenario: post condition
    Given user is navigated on herokuapp web page
    When user search for 'IBM_TEST' computer name
    And user click on 'IBM_TEST' link in the table
    And user click on Delete this computer button
