$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("createComputerTest.feature");
formatter.feature({
  "line": 2,
  "name": "Verify computer creation",
  "description": "I us user\r\nI want to create new computer\r\nTo make other manipulation with computers",
  "id": "verify-computer-creation",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@create"
    }
  ]
});
formatter.before({
  "duration": 5999494716,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Computer creation",
  "description": "",
  "id": "verify-computer-creation;computer-creation",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "user click on Add a new computer button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "user fill Computer Name field with \u0027IBM_TEST\u0027 name",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user fill Introduced date with \u00272018-03-16\u0027 date",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user fill Discontinued date with \u00272018-03-15\u0027 date",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user select \u0027Sony\u0027 company from Company drop-down",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user click on Create/Save this computer name button",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user see \u0027Done! Computer IBM_TEST has been createddddd\u0027 message",
  "keyword": "Then "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 229983787,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnAddANewComputerButton()"
});
formatter.result({
  "duration": 286142646,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 36
    }
  ],
  "location": "webStepDefinitions.userFillComputerNameFieldWithName(String)"
});
formatter.result({
  "duration": 321354526,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-16",
      "offset": 32
    }
  ],
  "location": "webStepDefinitions.userFillIntroducedDateWithYearMonthAndDay(String)"
});
formatter.result({
  "duration": 135472304,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-15",
      "offset": 34
    }
  ],
  "location": "webStepDefinitions.userFillDiscontinuedDateWithYearMonthAndDay(String)"
});
formatter.result({
  "duration": 138697196,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sony",
      "offset": 13
    }
  ],
  "location": "webStepDefinitions.userSelectSonyCompanyFromCompanyDropDown(String)"
});
formatter.result({
  "duration": 62892296,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnCreateThisComputerName()"
});
formatter.result({
  "duration": 473315535,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Done! Computer IBM_TEST has been createddddd",
      "offset": 10
    }
  ],
  "location": "webStepDefinitions.userSeeDoneComputerIBMHasBeenCreatedMessage(String)"
});
formatter.result({
  "duration": 79840853,
  "error_message": "java.lang.AssertionError: Message is not recognized\nExpected: is \"Done! Computer IBM_TEST has been createddddd\"\n     but: was \"Done! Computer IBM_TEST has been created\"\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:956)\r\n\tat webStepDefinition.webStepDefinitions.userSeeDoneComputerIBMHasBeenCreatedMessage(webStepDefinitions.java:89)\r\n\tat ✽.Then user see \u0027Done! Computer IBM_TEST has been createddddd\u0027 message(createComputerTest.feature:15)\r\n",
  "status": "failed"
});
formatter.write("Current Page URL is http://computer-database.herokuapp.com/computers");
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1160138003,
  "status": "passed"
});
formatter.before({
  "duration": 4467352468,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Verify computer parameters",
  "description": "",
  "id": "verify-computer-creation;verify-computer-parameters",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "user search for \u0027IBM_TEST\u0027 computer name",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "user see that computer with \u0027IBM_TEST\u0027 name is present in computer table",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "user see that computer with \u0027IBM_TEST\u0027 name has \u00272018-03-16\u0027 date as Introduced date",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "user see that computer with \u0027IBM_TEST\u0027 name has \u00272018-03-15\u0027 date as Discontinued date",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user see that computer with \u0027IBM_TEST\u0027 name has \u0027Sony\u0027 company name",
  "keyword": "And "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 1766718,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 17
    }
  ],
  "location": "webStepDefinitions.userSearchForName(String)"
});
formatter.result({
  "duration": 756603462,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 29
    }
  ],
  "location": "webStepDefinitions.userSeeThatComputerWithNameIsPresentInComputerTable(String)"
});
formatter.result({
  "duration": 32066000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 29
    },
    {
      "val": "2018-03-16",
      "offset": 49
    }
  ],
  "location": "webStepDefinitions.userSeeThatComputerWithNameHasYearMonthAndDayAsIntroducedDate(String,String)"
});
formatter.result({
  "duration": 52583943,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 29
    },
    {
      "val": "2018-03-15",
      "offset": 49
    }
  ],
  "location": "webStepDefinitions.userSeeThatComputerWithNameHasYearMonthAndDayAsDiscontinuedDate(String,String)"
});
formatter.result({
  "duration": 43838454,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 29
    },
    {
      "val": "Sony",
      "offset": 49
    }
  ],
  "location": "webStepDefinitions.userSeeThatComputerWithIBMNameHasSonyCompanyName(String,String)"
});
formatter.result({
  "duration": 42419787,
  "status": "passed"
});
formatter.after({
  "duration": 751820840,
  "status": "passed"
});
formatter.before({
  "duration": 4352833859,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "post condition",
  "description": "",
  "id": "verify-computer-creation;post-condition",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 26,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 27,
  "name": "user search for \u0027IBM_TEST\u0027 computer name",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "user click on \u0027IBM_TEST\u0027 link in the table",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "user click on Delete this computer button",
  "keyword": "And "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 567309,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 17
    }
  ],
  "location": "webStepDefinitions.userSearchForName(String)"
});
formatter.result({
  "duration": 677662955,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 15
    }
  ],
  "location": "webStepDefinitions.userClickOnNameLinkInTheTable(String)"
});
formatter.result({
  "duration": 251347556,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnDeleteThisComputerButton()"
});
formatter.result({
  "duration": 368642266,
  "status": "passed"
});
formatter.after({
  "duration": 783113705,
  "status": "passed"
});
formatter.uri("deleteComputerTest.feature");
formatter.feature({
  "line": 2,
  "name": "Verify computer deletion",
  "description": "I as user\r\nI want to be able delete computer\r\nTo get rid of computer if it is not needed",
  "id": "verify-computer-deletion",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@delete"
    }
  ]
});
formatter.before({
  "duration": 4373773727,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Computer creation",
  "description": "",
  "id": "verify-computer-deletion;computer-creation",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "user click on Add a new computer button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "user fill Computer Name field with \u0027IBM_TEST\u0027 name",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user fill Introduced date with \u00272018-03-16\u0027 date",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user fill Discontinued date with \u00272018-03-15\u0027 date",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user select \u0027Sony\u0027 company from Company drop-down",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user click on Create/Save this computer name button",
  "keyword": "And "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 454717,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnAddANewComputerButton()"
});
formatter.result({
  "duration": 428038264,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 36
    }
  ],
  "location": "webStepDefinitions.userFillComputerNameFieldWithName(String)"
});
formatter.result({
  "duration": 279264221,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-16",
      "offset": 32
    }
  ],
  "location": "webStepDefinitions.userFillIntroducedDateWithYearMonthAndDay(String)"
});
formatter.result({
  "duration": 120837231,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-15",
      "offset": 34
    }
  ],
  "location": "webStepDefinitions.userFillDiscontinuedDateWithYearMonthAndDay(String)"
});
formatter.result({
  "duration": 136521984,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sony",
      "offset": 13
    }
  ],
  "location": "webStepDefinitions.userSelectSonyCompanyFromCompanyDropDown(String)"
});
formatter.result({
  "duration": 80158483,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnCreateThisComputerName()"
});
formatter.result({
  "duration": 367389525,
  "status": "passed"
});
formatter.after({
  "duration": 746623010,
  "status": "passed"
});
formatter.before({
  "duration": 4354473366,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "computer deletion",
  "description": "",
  "id": "verify-computer-deletion;computer-deletion",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 17,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "user search for \u0027IBM_TEST\u0027 computer name",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "user click on \u0027IBM_TEST\u0027 link in the table",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "user click on Delete this computer button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "user see \u0027Done! Computer has been deleted\u0027 message",
  "keyword": "Then "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 629729,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 17
    }
  ],
  "location": "webStepDefinitions.userSearchForName(String)"
});
formatter.result({
  "duration": 641124062,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 15
    }
  ],
  "location": "webStepDefinitions.userClickOnNameLinkInTheTable(String)"
});
formatter.result({
  "duration": 254019361,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnDeleteThisComputerButton()"
});
formatter.result({
  "duration": 365714462,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Done! Computer has been deleted",
      "offset": 10
    }
  ],
  "location": "webStepDefinitions.userSeeDoneComputerIBMHasBeenCreatedMessage(String)"
});
formatter.result({
  "duration": 49740681,
  "status": "passed"
});
formatter.after({
  "duration": 772930981,
  "status": "passed"
});
formatter.uri("updateComputerTest.feature");
formatter.feature({
  "line": 2,
  "name": "Verify computer edition",
  "description": "I us user\r\nI want to be able edit computer data\r\nTo be able operate computer content",
  "id": "verify-computer-edition",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@update"
    }
  ]
});
formatter.before({
  "duration": 4289349759,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "pre condition",
  "description": "",
  "id": "verify-computer-edition;pre-condition",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "user click on Add a new computer button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "user fill Computer Name field with \u0027IBM_TEST\u0027 name",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user fill Introduced date with \u00272018-03-16\u0027 date",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user fill Discontinued date with \u00272018-03-15\u0027 date",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user select \u0027Sony\u0027 company from Company drop-down",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user click on Create/Save this computer name button",
  "keyword": "And "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 550322,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnAddANewComputerButton()"
});
formatter.result({
  "duration": 401977207,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 36
    }
  ],
  "location": "webStepDefinitions.userFillComputerNameFieldWithName(String)"
});
formatter.result({
  "duration": 310071159,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-16",
      "offset": 32
    }
  ],
  "location": "webStepDefinitions.userFillIntroducedDateWithYearMonthAndDay(String)"
});
formatter.result({
  "duration": 126609877,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-15",
      "offset": 34
    }
  ],
  "location": "webStepDefinitions.userFillDiscontinuedDateWithYearMonthAndDay(String)"
});
formatter.result({
  "duration": 151342737,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sony",
      "offset": 13
    }
  ],
  "location": "webStepDefinitions.userSelectSonyCompanyFromCompanyDropDown(String)"
});
formatter.result({
  "duration": 77441641,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnCreateThisComputerName()"
});
formatter.result({
  "duration": 382358820,
  "status": "passed"
});
formatter.after({
  "duration": 799466904,
  "status": "passed"
});
formatter.before({
  "duration": 4295311640,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "computer edition",
  "description": "",
  "id": "verify-computer-edition;computer-edition",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 17,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "user search for \u0027IBM_TEST\u0027 computer name",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "user click on \u0027IBM_TEST\u0027 link in the table",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "user fill Computer Name field with \u0027IBM_TEST_EDITED\u0027 name",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "user fill Discontinued date with \u00272017-04-16\u0027 date",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "user fill Introduced date with \u00272017-04-17\u0027 date",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user select \u0027Nokia\u0027 company from Company drop-down",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "user click on Create/Save this computer name button",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "user see \u0027Done! Computer IBM_TEST_EDITED has been updated\u0027 message",
  "keyword": "Then "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 558222,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 17
    }
  ],
  "location": "webStepDefinitions.userSearchForName(String)"
});
formatter.result({
  "duration": 1008608797,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST",
      "offset": 15
    }
  ],
  "location": "webStepDefinitions.userClickOnNameLinkInTheTable(String)"
});
formatter.result({
  "duration": 249503012,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST_EDITED",
      "offset": 36
    }
  ],
  "location": "webStepDefinitions.userFillComputerNameFieldWithName(String)"
});
formatter.result({
  "duration": 200352949,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2017-04-16",
      "offset": 34
    }
  ],
  "location": "webStepDefinitions.userFillDiscontinuedDateWithYearMonthAndDay(String)"
});
formatter.result({
  "duration": 130593683,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2017-04-17",
      "offset": 32
    }
  ],
  "location": "webStepDefinitions.userFillIntroducedDateWithYearMonthAndDay(String)"
});
formatter.result({
  "duration": 121616689,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nokia",
      "offset": 13
    }
  ],
  "location": "webStepDefinitions.userSelectSonyCompanyFromCompanyDropDown(String)"
});
formatter.result({
  "duration": 116143499,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnCreateThisComputerName()"
});
formatter.result({
  "duration": 362823398,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Done! Computer IBM_TEST_EDITED has been updated",
      "offset": 10
    }
  ],
  "location": "webStepDefinitions.userSeeDoneComputerIBMHasBeenCreatedMessage(String)"
});
formatter.result({
  "duration": 49306508,
  "status": "passed"
});
formatter.after({
  "duration": 780720814,
  "status": "passed"
});
formatter.before({
  "duration": 4887272997,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Verify computer parameters",
  "description": "",
  "id": "verify-computer-edition;verify-computer-parameters",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 28,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "user search for \u0027IBM_TEST_EDITED\u0027 computer name",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "user see that computer with \u0027IBM_TEST_EDITED\u0027 name is present in computer table",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "user see that computer with \u0027IBM_TEST_EDITED\u0027 name has \u00272017-04-17\u0027 date as Introduced date",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "user see that computer with \u0027IBM_TEST_EDITED\u0027 name has \u00272017-04-16\u0027 date as Discontinued date",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "user see that computer with \u0027IBM_TEST_EDITED\u0027 name has \u0027Nokia\u0027 company name",
  "keyword": "And "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 631309,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST_EDITED",
      "offset": 17
    }
  ],
  "location": "webStepDefinitions.userSearchForName(String)"
});
formatter.result({
  "duration": 848710596,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST_EDITED",
      "offset": 29
    }
  ],
  "location": "webStepDefinitions.userSeeThatComputerWithNameIsPresentInComputerTable(String)"
});
formatter.result({
  "duration": 27187775,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST_EDITED",
      "offset": 29
    },
    {
      "val": "2017-04-17",
      "offset": 56
    }
  ],
  "location": "webStepDefinitions.userSeeThatComputerWithNameHasYearMonthAndDayAsIntroducedDate(String,String)"
});
formatter.result({
  "duration": 48276582,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST_EDITED",
      "offset": 29
    },
    {
      "val": "2017-04-16",
      "offset": 56
    }
  ],
  "location": "webStepDefinitions.userSeeThatComputerWithNameHasYearMonthAndDayAsDiscontinuedDate(String,String)"
});
formatter.result({
  "duration": 58214762,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST_EDITED",
      "offset": 29
    },
    {
      "val": "Nokia",
      "offset": 56
    }
  ],
  "location": "webStepDefinitions.userSeeThatComputerWithIBMNameHasSonyCompanyName(String,String)"
});
formatter.result({
  "duration": 54096635,
  "status": "passed"
});
formatter.after({
  "duration": 795796776,
  "status": "passed"
});
formatter.before({
  "duration": 4318228795,
  "status": "passed"
});
formatter.scenario({
  "line": 35,
  "name": "post condition",
  "description": "",
  "id": "verify-computer-edition;post-condition",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 36,
  "name": "user is navigated on herokuapp web page",
  "keyword": "Given "
});
formatter.step({
  "line": 37,
  "name": "user search for \u0027IBM_TEST_EDITED\u0027 computer name",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "user click on \u0027IBM_TEST_EDITED\u0027 link in the table",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "user click on Delete this computer button",
  "keyword": "And "
});
formatter.match({
  "location": "webStepDefinitions.userIsNavigatedOnHerokuappWebPage()"
});
formatter.result({
  "duration": 637630,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST_EDITED",
      "offset": 17
    }
  ],
  "location": "webStepDefinitions.userSearchForName(String)"
});
formatter.result({
  "duration": 999675654,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "IBM_TEST_EDITED",
      "offset": 15
    }
  ],
  "location": "webStepDefinitions.userClickOnNameLinkInTheTable(String)"
});
formatter.result({
  "duration": 262499367,
  "status": "passed"
});
formatter.match({
  "location": "webStepDefinitions.userClickOnDeleteThisComputerButton()"
});
formatter.result({
  "duration": 528959233,
  "status": "passed"
});
formatter.after({
  "duration": 749312592,
  "status": "passed"
});
});